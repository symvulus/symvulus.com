This is a [Next.js](https://nextjs.org/) project for [symvulus.com](https://symvulus.com).

## Getting Started

First, run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

## Directory Structure

    .
    ├── cdk/                    # AWS CDK deployment
    ├── components/             # App components
    ├── lib/                    # App helper scripts
    ├── pages/                  # App routes
    ├── posts/                  # App blog posts
    ├── public/                 # App static files
    └── styles/                 # App stylesheets
