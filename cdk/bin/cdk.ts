#!/usr/bin/env node
import "source-map-support/register";
import { App } from "aws-cdk-lib";
import { Builder } from "@sls-next/lambda-at-edge";
import { CdkStack } from "../lib/cdk-stack";
import { join } from "path";

const nextConfigPath = process.cwd();
const outputDir = join(nextConfigPath, ".serverless_nextjs");
export const builder = new Builder(nextConfigPath, outputDir, { args: ["build"] });

const stackId = process.env.CDK_STACK_NAME;

if (stackId) {
	builder.build()
		.then(() => {
			const app = new App();
			new CdkStack(app, stackId, {
				env: {
					account: process.env.CDK_DEFAULT_ACCOUNT,
					region: "us-east-1"
				}
			});
		})
		.catch((e) => {
			throw e;
		});
}
