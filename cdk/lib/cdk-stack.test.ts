import { App } from "aws-cdk-lib";
import { CdkStack } from "./cdk-stack";
import { SynthUtils } from "@aws-cdk/assert";

expect.addSnapshotSerializer({
	test: val => typeof val === "string",
	print: val => {
		let value: string = val as string;
		value = value.replace(/(AssetParameters)[A-Fa-f0-9]{64}(\w+)[A-Fa-f0-9]{8}/, "$1{64HASH}$2{8HASH}");
		value = value.replace(/(\w+) (\w+) for asset\s?(version)?\s?"([A-Fa-f0-9]{64})"/, "{64HASH}");
		value = value.replace(/(CurrentVersion)[A-Fa-f0-9]{40}/, "$1{40HASH}");
		value = value.replace(/[A-Fa-f0-9]{64}\.zip/, "{64HASH}.zip");
		return `"${value}"`;
	}
});

describe("TestStack", () => {
	it("renders", () => {
		const app = new App();
		const stack = new CdkStack(app, "TestStack", { env: { region: "us-east-1" } });
		expect(SynthUtils.toCloudFormation(stack)).toMatchSnapshot();
	});

	it("renders with domain name and zone domain name", () => {
		process.env.DOMAIN_NAME = "subdomain.example.com";
		process.env.ZONE_NAME = "example.com";
		process.env.ZONE_ID = "ZOJJZC49E0EPZ";

		const app = new App();
		const stack = new CdkStack(app, "TestStack", { env: { region: "us-east-1" } });
		expect(SynthUtils.toCloudFormation(stack)).toMatchSnapshot();
	});
});
