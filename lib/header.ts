export function getTitle(title: string): string {
	return `${title} | Symvulus`;
}
