import { readFile, readdir } from "fs/promises";
import { join } from "path";
import matter from "gray-matter";

const postsDirectory = join(process.cwd(), "posts");

export type Post = {
	id: string;
	slug: string;
	data: matter.GrayMatterFile<matter.Input>["data"];
	content: matter.GrayMatterFile<matter.Input>["content"];
};

export async function getPosts(): Promise<Post[]> {
	// eslint-disable-next-line security/detect-non-literal-fs-filename -- false positive: no user input
	const filenames = await readdir(postsDirectory);

	const posts = filenames.map(async (filename) => {
		const id = filename.replace(/\.md$/, "");
		return getPost(id);
	});

	return await Promise.all(posts);
}

export async function getPost(id: string): Promise<Post> {
	const slug = `/blog/${id}`;
	const filePath = join(postsDirectory, `${id}.md`);
	// eslint-disable-next-line security/detect-non-literal-fs-filename -- false positive: no user input
	const fileContents = await readFile(filePath, "utf8");
	const { data, content } = matter(fileContents);

	const post: Post = { id, slug, data, content };

	return post;
}
