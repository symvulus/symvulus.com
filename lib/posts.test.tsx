import { getPosts } from "./posts";

describe("getPosts()", () => {
	it("should return posts", async () => {
		const posts = await getPosts();
		posts.forEach((post) => {
			expect(post).toStrictEqual(
				expect.objectContaining({
					id: expect.any(String),
					slug: expect.stringMatching(/\/blog\/[a-z0-9]+(?:-[a-z0-9]+)*?/),
					data: expect.objectContaining({
						author: expect.objectContaining({
							name: expect.any(String)
						}),
						date: expect.stringMatching(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{4}/),
						excerpt: expect.any(String),
						title: expect.any(String)
					}),
					content: expect.any(String)
				})
			);
		});
	});
});
