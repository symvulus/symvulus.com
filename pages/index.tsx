import { Col, Container, Row } from "react-bootstrap";
import { FaEye, FaInfoCircle, FaRocket, FaSyncAlt } from "react-icons/fa";
import Header from "../components/Header";
import type { PageProps } from "../components/Header";
import Tile from "../components/Tile";
import { getTitle } from "../lib/header";

export default function Home(): JSX.Element {
	const pageHeaderProps: PageProps = {
		title: getTitle("DevOps Consulting")
	};

	return (
		<>
			<Header title={pageHeaderProps.title} />
			<div className="bg-light">
				<Container className="py-5 text-center" style={{ fontSize: "1.4rem", lineHeight: "2em" }}>
					<Row>
						<Col>
							Empowering teams to be <span className="fw-bold">self-sufficient</span> in getting their <span className="fw-bold">products to market faster</span> by optimizing the software development life cycle and providing <span className="fw-bold">cloud architecture</span> and <span className="fw-bold">continuous integration and delivery</span> expertise.
						</Col>
					</Row>
				</Container>
			</div>
			<Container className="py-5">
				<Row>
					<Col className="text-center">
						<Tile icon={FaEye} href="#assessment">Assessment</Tile>
						<Tile icon={FaInfoCircle} href="#consultation">Consultation</Tile>
						<Tile icon={FaRocket} href="#implementation">Implementation</Tile>
						<Tile icon={FaSyncAlt} href="#continuity">Continuity</Tile>
					</Col>
				</Row>
			</Container>
			<div style={{ backgroundColor: "#e1e2e1" }}>
				<Container className="py-4">
					<Row>
						<Col />
						<Col xs="8" className="pt-5 pb-4">
							<p id="assessment">
								We will <span className="fw-bold">assess</span> the current state of your software development lifecycle including the supporting infrastructure,
								and provide general future state recommendations for several categories such as automation, monitoring, and cost efficiency.
							</p>
							<p id="consultation">
								After our teams come to a consensus on the priorities and a build a comprehensive roadmap together,
								we will provide more detailed <span className="fw-bold">consultation</span> tailored specifically for your workflow.
							</p>
							<p id="implementation">
								Our team will also <span className="fw-bold">implement</span> easy-to-maintain, end-to-end solutions to optimize the efficiency and stability of your architecture.
							</p>
							<p id="continuity">
								<span className="fw-bold">Continuity</span> is provided differently based on your business requirements,
								but usually consists of continuing to implement solutions long-term or by training your technical leaders to adopt the DevOps mentality.
							</p>
						</Col>
					</Row>
				</Container>
			</div>
		</>
	);
}
