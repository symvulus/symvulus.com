import type { GetStaticProps, InferGetStaticPropsType } from "next";
import Header from "../../components/Header";
import type { PageProps } from "../../components/Header";
import type { Post } from "../../lib/posts";
import { getPosts } from "../../lib/posts";
import { getTitle } from "../../lib/header";

export default function Blog({ posts }: InferGetStaticPropsType<typeof getStaticProps>): JSX.Element {
	const pageHeaderProps: PageProps = {
		title: getTitle("Blog")
	};

	return (
		<>
			<Header title={pageHeaderProps.title} />
			<div className="container">
				<ul>
					{posts.map((post: Post) => (
						<li key={post.slug}>{post.data.title}</li>
					))}
				</ul>
			</div>
		</>
	);
}

export const getStaticProps: GetStaticProps = async () => {
	const posts = await getPosts();

	return { props: { posts } };
};
