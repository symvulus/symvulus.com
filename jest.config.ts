import type { Config } from "@jest/types";

const config: Config.InitialOptions = {
	collectCoverageFrom: [
		"**/*.{js,jsx,ts,tsx}"
	],
	coveragePathIgnorePatterns: [
		"/cdk/",
		"/cdk.out/",
		"/jest.*.{js,ts}",
		"/next.config.js",
		"/next-sitemap.js",
		"/node_modules/",
		"/pages/_app.{js,jsx,ts,tsx}"
	],
	coverageReporters: [
		"text",
		"html",
		"cobertura"
	],
	reporters: [
		"default",
		[
			"jest-junit",
			{
				suiteName: "NextJS",
				classNameTemplate: "{classname}"
			}
		]
	],
	moduleNameMapper: {
		"\\.module\\.(css|sass|scss)$": "identity-obj-proxy"
	},
	testPathIgnorePatterns: [
		"/cdk/",
		"/cdk.out/",
		"/node_modules/"
	],
	testEnvironment: "jsdom",
	transform: {
		"^.+\\.(js|jsx|ts|tsx)$": "babel-jest"
	}
};

export default config;
