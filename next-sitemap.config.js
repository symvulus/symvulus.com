module.exports = {
	siteUrl: "https://symvulus.com",
	generateRobotsTxt: true,
	outDir: ".serverless_nextjs/assets/public"
}
