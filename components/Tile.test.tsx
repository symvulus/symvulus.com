import { FaUser } from "react-icons/fa";
import Tile from "./Tile";
import { create } from "react-test-renderer";

describe("Tile component", () => {
	it("should render", () => {
		const component = create(<Tile icon={FaUser} href="/user">User</Tile>).toJSON();
		expect(component).toMatchSnapshot();
	});
});
