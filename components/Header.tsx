import { Col, Container, Nav, Navbar, Row } from "react-bootstrap";
import Head from "next/head";
import Link from "next/link";

export type PageProps = {
	title: string;
};

export default function Header({ title }: PageProps): JSX.Element {
	return (
		<div className="position-relative">
			<Head>
				<title>{title}</title>
				<link rel="icon" href="/favicon.ico" />
				<meta name="viewport" content="initial-scale=1.0, width=device-width" />
			</Head>
			<div className="bg-secondary position-absolute end-0 top-0 h-100 w-50" />
			<Container>
				<Row>
					<Col md="auto" className="bg-white position-relative">
						<Row>
							<Col>
								<Navbar expand="lg" variant="light" className="my-2">
									<Link href="/" passHref>
										<Navbar.Brand><span className="mb-0 h2">S</span></Navbar.Brand>
									</Link>
									<Navbar.Toggle aria-controls="navbar-nav" />
									<Navbar.Collapse id="navbar-nav">
										<Nav className="navbar-nav">
											<Link href="/blog" passHref><Nav.Link className="mx-2">Blog</Nav.Link></Link>
											<Link href="#contact" passHref><Nav.Link className="mx-2">Contact</Nav.Link></Link>
										</Nav>
									</Navbar.Collapse>
								</Navbar>
							</Col>
						</Row>
						<Row className="my-5">
							<Col>
								<div className="mb-5 mt-4 p-5 rounded-3" style={{ backgroundColor: "#e9ecef" }}>
									<h1 className="fw-bold pe-5" style={{ fontSize: "3rem" }}>
										We help get products<br />to market faster.
									</h1>
								</div>
							</Col>
						</Row>
						<div className="bg-secondary position-absolute end-0 top-0 h-100 w-100" style={{ transform: "skew(-10deg) translateX(100%)", transformOrigin: "top" }} />
					</Col>
					<Col />
				</Row>
			</Container>
		</div>
	);
}
