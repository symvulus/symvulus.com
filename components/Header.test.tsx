import Header from "./Header";
import type { PageProps } from "./Header";
import { create } from "react-test-renderer";

const HeaderProps: PageProps = {
	title: "Home"
};

describe("Header component", () => {
	it("should render", () => {
		const component = create(<Header title={HeaderProps.title} />).toJSON();
		expect(component).toMatchSnapshot();
	});
});
