import type { IconType } from "react-icons";
import Link from "next/link";
import type { PropsWithChildren } from "react";

export type TileProps = {
	icon: IconType;
	href: string;
};

export default function Tile({ icon, href, children }: PropsWithChildren<TileProps>): JSX.Element {
	const Icon = icon;
	return (
		<Link href={href}>
			<a className="bg-light d-inline-block m-1 py-4 text-center text-dark text-decoration-none" style={{ width: "16rem" }}>
				<Icon className="text-secondary h1 my-4" />
				<div className="fw-bold my-2">
					{children}
				</div>
			</a>
		</Link>
	);
}
