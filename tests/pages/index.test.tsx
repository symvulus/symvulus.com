import Home from "../../pages";
import { create } from "react-test-renderer";

describe("Home page", () => {
	it("should render", () => {
		const page = create(<Home />).toJSON();
		expect(page).toMatchSnapshot();
	});
});
