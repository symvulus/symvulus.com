import { builder } from "./cdk/bin/cdk";

module.exports = async (): Promise<void> => {
	await builder.build();
};
