import type { Config } from "@jest/types";

const config: Config.InitialOptions = {
	collectCoverageFrom: [
		"cdk/**/*.{js,ts}"
	],
	coverageReporters: [
		"text",
		"html",
		"cobertura"
	],
	globalSetup: "./jest.cdk.setup.ts",
	reporters: [
		"default",
		[
			"jest-junit",
			{
				suiteName: "CDK",
				classNameTemplate: "{classname}"
			}
		]
	],
	testRegex: [
		"/cdk/.+\\.(test|spec)\\.[jt]s$"
	]
};

export default config;
